package ru.kpfu.itis.user.model;


import lombok.*;

import javax.persistence.*;

//@Entity(name = "band_user")
//@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class BandUser {

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "user_id")
    private User user;

//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "band_id")
    private Band band;
    private String description;
}
