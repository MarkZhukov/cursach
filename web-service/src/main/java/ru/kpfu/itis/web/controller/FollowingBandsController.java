package ru.kpfu.itis.web.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FollowingBandsController {

    @GetMapping("/followingbands")
    public String followingBandsPage(){

        return "following-bands";
    }
}
