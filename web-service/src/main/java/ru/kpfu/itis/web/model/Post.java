package ru.kpfu.itis.web.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kpfu.itis.web.dto.UserDto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "post")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String text;
    private String ownerImg;
    private String ownerName;
    private String imgUrl;


    public Post(String text, UserDto owner, String imgUrl) {
        this.text = text;
        this.ownerImg = owner.getAvatar();
        this.ownerName = owner.getUsername();
        this.imgUrl = imgUrl;
    }

    public Post(String text, User owner, String imgUrl) {
        this.text = text;
        this.ownerImg = owner.getAvatar();
        this.ownerName = owner.getUsername();
        this.imgUrl = imgUrl;
    }
}
