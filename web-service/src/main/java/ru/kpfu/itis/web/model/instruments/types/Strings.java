package ru.kpfu.itis.web.model.instruments.types;

import ru.kpfu.itis.web.model.instruments.UnderType;

public enum Strings implements UnderType {
    GUITAR,
    ELECTRICGUITAR,
    BASSGUITAR,
    UKULELE,
    VIOLEN,
    CELLO,
    COUNTERBASS,
    DOMRA,
    BALALAIKA,

}
