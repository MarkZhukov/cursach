package ru.kpfu.itis.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.web.dto.UserAuthDto;
import ru.kpfu.itis.web.service.AuthService;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private MessageService messageService;

    @Override
    public void authUser(UserAuthDto dto) {
        messageService.authUser(dto);
    }
}
