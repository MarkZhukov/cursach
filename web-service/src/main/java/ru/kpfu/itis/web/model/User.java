package ru.kpfu.itis.web.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity(name = "app_user")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;
    private String surname;
    private String name;
    private String about;
    private String password;
    private String avatar;
    private String avatar124;
    private String back;
    private String country;
    private String city;

    public User(String imgUrl, String username) {
        this.avatar = imgUrl;
        this.username = username;
    }

    public User(String username,
                String about,
                String avatar,
                String back,
                String name,
                String surname,
                String country,
                String city,
                String avatar124) {
        this.name = name;
        this.avatar = avatar;
        this.back = back;
        this.username = username;
        this.surname = surname;
        this.country = country;
        this.city = city;
        this.about = about;
        this.avatar124 = avatar124;
    }



//    @Column(nullable = false)
//    private boolean inSearch;





}
