package ru.kpfu.itis.user.service.impl;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.user.dto.UserDto;
import ru.kpfu.itis.user.model.Post;
import ru.kpfu.itis.user.service.PostService;
import ru.kpfu.itis.user.service.UserService;

@Component
public class MessageConsumer {

    @Autowired
    private UserService userService;

    @Autowired
    private PostService postService;

    @RabbitListener(queues = "${messaging.user.queue}")
    public void process(UserDto dto) {
        userService.save(dto);
    }

    @RabbitListener(queues = "${messaging.post.queue}")
    public void processPost(Post post) {
        postService.save(post);
    }


}
