package ru.kpfu.itis.user.model.role;

public enum RoleInBand {
    ADMIN,
    MODERATOR,
    PART
}
