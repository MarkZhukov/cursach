package ru.kpfu.itis.web.model.instruments.types;


import ru.kpfu.itis.web.model.instruments.UnderType;

public enum KeyBoard implements UnderType {
    PIANO,
    SYNTH
}
