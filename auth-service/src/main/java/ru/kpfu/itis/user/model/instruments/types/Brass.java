package ru.kpfu.itis.user.model.instruments.types;

import ru.kpfu.itis.user.model.instruments.PodType;

public enum Brass implements PodType {
    SAXSOPHONE,
    TRUMPET,
    TROMBONE,
    TUBA,
    FRENCHHORN,
    FLUTE
}
