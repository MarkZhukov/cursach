package ru.kpfu.itis.user.model.instruments.types;

import ru.kpfu.itis.user.model.instruments.PodType;

public enum Percussion implements PodType {
    DRUMS,
    ORCESTRAL,
}
