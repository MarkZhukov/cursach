package ru.kpfu.itis.web.model.instruments.types;

import ru.kpfu.itis.web.model.instruments.UnderType;

public enum Brass implements UnderType {
    SAXSOPHONE,
    TRUMPET,
    TROMBONE,
    TUBA,
    FRENCHHORN,
    FLUTE
}
