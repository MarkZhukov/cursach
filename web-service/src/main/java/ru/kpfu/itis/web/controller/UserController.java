package ru.kpfu.itis.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.web.dto.UserDto;
import ru.kpfu.itis.web.model.Post;
import ru.kpfu.itis.web.service.PostService;
import ru.kpfu.itis.web.service.UserService;

import java.util.List;

@Controller
@RequestMapping("/interface")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private PostService postService;

    @GetMapping
    public String renderRegisterUserPage() {
        return "main";
    }

    @PostMapping("/registeration")
    public String registerUser(UserDto userDto) {
        userService.registerUser(userDto);
        return "success";
    }

    @GetMapping("/userslist")
    public String renderAllUsersPage(Model model) {
        List<UserDto> users = userService.getAll();
        List<Post> posts = postService.getAll();
        model.addAttribute("users", users);
        model.addAttribute("posts", posts);

        return "users";
    }
}
