package ru.kpfu.itis.user.model.instruments.types;

import ru.kpfu.itis.user.model.instruments.PodType;

public enum Vocals implements PodType {
    POP,
    ACADEM,
    JAZZ,
    ROCK,
    EXTREME,
    HIPHOP
}
