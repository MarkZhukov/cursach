package ru.kpfu.itis.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.user.dto.UserDto;
import ru.kpfu.itis.user.model.User;
import ru.kpfu.itis.user.repository.UserRepository;
import ru.kpfu.itis.user.service.UserService;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }



    @Override
    public void save(UserDto userDto) {
        User user = User.builder()
                .username(userDto.getUsername())
                .name(userDto.getName())
                .surname(userDto.getSurname())
                .about(userDto.getAbout())
                .password(userDto.getPassword())
                .avatar(userDto.getAvatar())
                .avatar124(userDto.getAvatar124())
                .back(userDto.getBack())
                .country(userDto.getCountry())
                .city(userDto.getCity())
                .build();

//        User user1 = User.builder()
//                .imgUrl(userDto.getImgUrl())
//                .name(userDto.getName())
//                .build();
        userRepository.save(user);
    }

    @Override
    public User getOne(String username) {
        return userRepository.findByUsername(username);
    }
}
