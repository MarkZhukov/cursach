package ru.kpfu.itis.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.web.dto.UserAuthDto;
import ru.kpfu.itis.web.dto.UserDto;
import ru.kpfu.itis.web.service.AuthService;

@Controller
@RequestMapping("/login")
public class AuthController {

    @Autowired
    public AuthService authService;

    @PostMapping
    public String auth(UserAuthDto dto){
        authService.authUser(dto);
        return "main";
    }


    @GetMapping
    public String loginPage(){
        return "login";
    }

}
