package ru.kpfu.itis.web.model.instruments.types;

import ru.kpfu.itis.web.model.instruments.UnderType;

public enum Vocals implements UnderType {
    POP,
    ACADEM,
    JAZZ,
    ROCK,
    EXTREME,
    HIPHOP
}
