package ru.kpfu.itis.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.kpfu.itis.web.model.Post;
import ru.kpfu.itis.web.service.PostService;

import java.util.Arrays;
import java.util.List;

@Service
public class PostServiceImpl implements PostService {


    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private MessageService messageService;

    @Value("${user.get-all.posturl}")
    private String getAllPostUrl;

    @Override
    public List<Post> getAll() {
        return Arrays.asList(restTemplate.getForObject(getAllPostUrl, Post[].class));
    }

    @Override
    public void create(Post post) {
        messageService.createPost(post);
    }

    @Override
    public List<Post> getForUsername(String username) {
        return Arrays.asList(restTemplate.getForObject(getAllPostUrl + "/" + username, Post[].class));

    }
}
