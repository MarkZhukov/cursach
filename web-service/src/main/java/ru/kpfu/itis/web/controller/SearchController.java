package ru.kpfu.itis.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.web.dto.UserDto;
import ru.kpfu.itis.web.model.Band;
import ru.kpfu.itis.web.service.SearchService;
import ru.kpfu.itis.web.service.UserService;

import java.util.List;

@Controller
@RequestMapping("/search")
public class SearchController {


    @Autowired
    private UserService userService;


    @GetMapping
    public String searchPage(@ModelAttribute("model")ModelMap model){
        List<UserDto> users = userService.getAll();
        model.addAttribute("users", users);
        return "search";
    }
}
