package ru.kpfu.itis.user.model;

/**
 * @author Mykola Yashchenko
 */
public enum Role {
    USER, ADMIN, SUPER_ADMIN
}
