package ru.kpfu.itis.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.web.dto.PostFormDto;
import ru.kpfu.itis.web.model.Post;
import ru.kpfu.itis.web.model.User;
import ru.kpfu.itis.web.service.PostService;

import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping("/")
public class NewsController {

    @Autowired
    private PostService postService;

    @GetMapping
    public String newsPage(@ModelAttribute("model")ModelMap model){
        List<Post> posts = postService.getAll();
        User user = new User("/img/author-main1.jpg","brity");
        User user1 = new User("/img/avatar5-sm.jpg","geerock");
//        posts.add(new Post("this guy made new great single, check it out",user,"/img/possst.jpg"));
//        postService.create(new Post("my band naw in search of good drums player",user,"/img/possst1.jpg"));
//
//        postService.create(new Post("Hey guys! We are gona be playing this Saturday of for their new Mystic Deer Party.\n" +
//                "                            If you wanna hang out and have a really good time, come and join us. We’l be waiting for you!",user1,"/img/post__thumb1.jpg"));
        model.addAttribute("avatar","/img/avatar.jpg");
        model.addAttribute("posts",posts);
        return "news";
    }

    @PostMapping
    public String createPost(@ModelAttribute("contactForm") PostFormDto postFormDto) {
        User user = new User("/img/avatar.jpg","micknamsom");
        Post post = Post.builder()
                .ownerName(user.getUsername())
                .ownerImg(user.getAvatar())
                .text(postFormDto.getText())
                .build();
        postService.create(post);
        return "news";
    }

}
