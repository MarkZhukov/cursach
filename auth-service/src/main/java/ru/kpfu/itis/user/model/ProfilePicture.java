package ru.kpfu.itis.user.model;

import entity.User;
import lombok.Getter;
import lombok.Setter;
import ua.social.network.entity.BaseEntity;

import javax.persistence.*;

/**
 * @author Mykola Yashchenko
 */
@Getter
@Setter
@Entity
@Table(name = "profile_pictures")
public class ProfilePicture extends BaseEntity {
    @Column(name = "url", nullable = false)
    private String url;

    @JoinColumn(name = "user_id", nullable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private User user;
}
