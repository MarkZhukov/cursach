package ru.kpfu.itis.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.user.model.Post;
import ru.kpfu.itis.user.model.User;
import ru.kpfu.itis.user.service.PostService;
import ru.kpfu.itis.user.service.UserService;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public ResponseEntity<List<User>> getAllUsers() {
        return ResponseEntity.ok(userService.getAll());
    }

    @Autowired
    PostService postService;

    @GetMapping("/posts")
    public ResponseEntity<List<Post>> getAllPosts() {
        return ResponseEntity.ok(postService.getAll());
    }

    @GetMapping("/posts/{username}")
    public ResponseEntity<List<Post>> getPostsForUser(@PathVariable("username") String username) {
        return ResponseEntity.ok(postService.getForUsername(username));
    }

    @GetMapping("/user/{username}")
    public ResponseEntity<User> getOneUser(@PathVariable("username") String username) {
        return ResponseEntity.ok(userService.getOne(username));
    }

}
