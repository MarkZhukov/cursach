package ru.kpfu.itis.user.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kpfu.itis.user.model.instruments.InstrumentType;
import ru.kpfu.itis.user.model.instruments.PodType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//@Entity(name = "instrument")
//@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Instrument {

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private InstrumentType instrumentType;

    private PodType podType;

    private String description;
}
