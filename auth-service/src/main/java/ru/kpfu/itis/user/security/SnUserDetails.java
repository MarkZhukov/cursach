package ru.kpfu.itis.user.security;

import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import ru.kpfu.itis.user.model.User;
import ua.social.network.authservice.entity.User;

import java.util.Collections;
import java.util.Optional;

@Getter
public class SnUserDetails extends org.springframework.security.core.userdetails.User {

    private String userId;

    public SnUserDetails(final User user) {
        super(user.getEmail(), user.getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + user.getRole().name())));

        this.userId = user.getId();
    }

    public SnUserDetails(Optional<User> user) {
        User user1 = user.get();
        super(user1.getEmail(), user1.getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + user1.getRole().name())));
        this.userId = user1.getId();
    }
}
