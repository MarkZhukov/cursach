package ru.kpfu.itis.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BandController {

    @GetMapping("/band")
    public String bandPage(){
        return "band";
    }

    @GetMapping("/band/about")
    public String bandAboutPage(){
        return "band-about";
    }

    @GetMapping("/band/subscribers")
    public String bandSubscribersPage(){
        return "band-subscribers";
    }
}
