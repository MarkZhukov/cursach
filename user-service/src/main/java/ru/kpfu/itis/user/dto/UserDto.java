package ru.kpfu.itis.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.*;

@Builder
@Getter
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private String username;
    private String surname;
    private String name;
    private String about;
    private String password;
    private String avatar;
    private String avatar124;
    private String back;
    private String country;
    private String city;




}
