package ru.kpfu.itis.user.model.instruments.types;

import ru.kpfu.itis.user.model.instruments.PodType;

public enum Creators implements PodType {

    RECORD,
    COMPONIST,
    BITMAKER,
    ARRANGEMENT,
    SVEDENIE,
    MASTERING
}
