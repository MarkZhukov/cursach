package ru.kpfu.itis.web.service;

import ru.kpfu.itis.web.dto.UserDto;

import java.util.List;

public interface UserService {

    void registerUser(UserDto userDto);
    UserDto getOne(String username);
    List<UserDto> getAll();
}
