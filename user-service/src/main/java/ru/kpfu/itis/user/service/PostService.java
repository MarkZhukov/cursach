package ru.kpfu.itis.user.service;

import ru.kpfu.itis.user.model.Post;

import java.util.List;

public interface PostService {

    List<Post> getAll();
    void save(Post post);

    List<Post> getForUsername(String username);
}
