package ru.kpfu.itis.web.model.instruments.types;

import ru.kpfu.itis.web.model.instruments.UnderType;

public enum Percussion implements UnderType {
    DRUMS,
    ORCESTRAL,
}
