package ru.kpfu.itis.web.service.impl;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.web.config.property.MessagingProperties;
import ru.kpfu.itis.web.dto.UserAuthDto;
import ru.kpfu.itis.web.dto.UserDto;
import ru.kpfu.itis.web.model.Post;

@Service
public class MessageService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private MessagingProperties messagingProperties;


    public void registerUser(UserDto userDto) {
        rabbitTemplate.convertAndSend(messagingProperties.getExchange(),
                messagingProperties.getUser().getRoutingKey(), userDto);
    }

    public void createPost(Post post) {
        rabbitTemplate.convertAndSend(messagingProperties.getExchange(),
                messagingProperties.getPost().getRoutingKey(), post);
    }

    public void authUser(UserAuthDto dto){

    }
}
