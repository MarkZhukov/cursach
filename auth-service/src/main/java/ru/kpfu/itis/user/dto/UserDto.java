package ru.kpfu.itis.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private String username;
    private String surname;
    private String firstname;
    private String email;
    private String password;
    private String sex;

}
