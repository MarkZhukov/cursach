package ru.kpfu.itis.user.model;


import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity(name = "band")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class Band {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private Photo photo;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User owner;
    private String country;
    private String city;
    private boolean inSearch;

    @ManyToMany(mappedBy = "bandParticipations")
    private Set<User> participants;

    @ManyToMany(mappedBy = "bandSubscriptions")
    private Set<User> subscribers;
    private List<Music> music;

}
