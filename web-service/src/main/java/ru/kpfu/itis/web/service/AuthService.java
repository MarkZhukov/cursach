package ru.kpfu.itis.web.service;

import ru.kpfu.itis.web.dto.UserAuthDto;
import ru.kpfu.itis.web.dto.UserDto;

public interface AuthService {
    void authUser(UserAuthDto dto);
}
