package ru.kpfu.itis.web.model.instruments.types;

import ru.kpfu.itis.web.model.instruments.UnderType;

public enum Creators implements UnderType {

    RECORD,
    COMPONIST,
    BITMAKER,
    ARRANGEMENT,
    SVEDENIE,
    MASTERING
}
