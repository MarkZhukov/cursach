package ru.kpfu.itis.web.service;

import ru.kpfu.itis.web.model.Band;

import java.util.List;

public interface SearchService {
    List<Band> getTopBands();

}
