<!DOCTYPE html>
<html>
<head>
    <title>users</title>
    <#--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">-->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
<body>
<div id="main-content" class="container">
    <div class="row">
        <div class="col-md-6">

                <#if users??>
                    <#list users as user>

                        <#--<td>${user.id}</td>-->
                            <td scope="row">${user.username}</td>
                            <td><img src="${user.avatar}" class="img-responsive img-thumbnail"></td>
                            <br>
                            <hr>
                    </#list>
                </#if>

                <#if posts??>
                    <#list posts as p>

                        <#--<td>${user.id}</td>-->
                            <td scope="row">${p.text}</td>
                            <td><img src="${p.imgUrl}" class="img-responsive img-thumbnail"></td>
                            <br>
                            <hr>
                    </#list>
                </#if>
        </div>
    </div>
</div>
</body>
</html>