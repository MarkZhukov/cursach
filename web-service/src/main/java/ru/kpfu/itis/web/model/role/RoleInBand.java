package ru.kpfu.itis.web.model.role;

public enum RoleInBand {
    ADMIN,
    MODERATOR,
    PART
}
