package ru.kpfu.itis.web.model.instruments;

public enum InstrumentType {
    VOCALS,
    STRINGS,
    PERCUSSION,
    KEYBOARDS,
    BRASS,
    CREATORS,
    OTHERS
}
