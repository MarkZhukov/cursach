package ru.kpfu.itis.web.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("music")
public class MusicController {


    @GetMapping
    public String musicPage() {

        return "music";
    }
}
