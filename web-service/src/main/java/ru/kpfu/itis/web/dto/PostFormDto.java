package ru.kpfu.itis.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostFormDto {
    private String text;
}
