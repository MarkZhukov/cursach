package ru.kpfu.itis.user.model.instruments;

public enum InstrumentType {
    VOCALS,
    STRINGS,
    PERCUSSION,
    KEYBOARDS,
    BRASS,
    CREATORS,
    OTHERS
}
