package ru.kpfu.itis.user.model.instruments.types;

import ru.kpfu.itis.user.model.instruments.PodType;

public enum Stringa implements PodType {
    GUITAR,
    ELECTRICGUITAR,
    BASSGUITAR,
    UKULELE,
    VIOLEN,
    CELLO,
    COUNTERBASS,
    DOMRA,
    BALALAIKA,

}
