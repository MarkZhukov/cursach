package ru.kpfu.itis.web.service;

import ru.kpfu.itis.web.model.Post;

import java.util.List;

public interface PostService {

    List<Post> getAll();
    void create(Post post);

    List<Post> getForUsername(String username);
}
