package ru.kpfu.itis.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.web.dto.UserDto;
import ru.kpfu.itis.web.model.Post;
import ru.kpfu.itis.web.model.User;
import ru.kpfu.itis.web.service.PostService;
import ru.kpfu.itis.web.service.UserService;

import java.util.LinkedList;
import java.util.List;

@Controller
public class ProfileController {

    @Autowired
    private PostService postService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/profile{username}", method = RequestMethod.GET)
    public String getProfilePage(@PathVariable("username") String username, @ModelAttribute("model")ModelMap model){


        List<Post> posts = postService.getForUsername(username);
        UserDto user = userService.getOne(username);

        model.addAttribute("posts",posts);
        model.addAttribute("userabout",user.getAbout());
        model.addAttribute("name",user.getName());
        model.addAttribute("surname",user.getSurname());
        model.addAttribute("country",user.getCountry());
        model.addAttribute("city",user.getCity());
        model.addAttribute("avatar",user.getAvatar());
        model.addAttribute("profon",user.getBack());
        model.addAttribute("av124",user.getAvatar124());




        return "profile";
    }

    void createdbata() {
        UserDto dtoo = UserDto.builder()
                .username("micknamsom")
                .name("Nick")
                .surname("Manson")
                .about("hi, im drumer")
                .password("lll")
                .country("USA")
                .city("Los-Angeles")
                .avatar("/img/avatar.jpg")
                .avatar124("/img/avatar124.jpg")
                .back("/img/profon.jpg")
                .build();
        userService.registerUser(dtoo);
        UserDto dto = UserDto.builder()
                .username("brity")
                .name("Jeremy")
                .surname("Jhons")
                .about("im mister dr")
                .password("lll")
                .avatar("/img/avatar6-sm.jpg")
                .avatar124("/img/avatar6.jpg")
                .back("/img/profon.jpg")
                .country("USA")
                .city("Los-Angeles")
                .build();
        userService.registerUser(dto);
        UserDto dto1 = UserDto.builder()
                .username("geerock")
                .name("Lex")
                .surname("Luter")
                .about("ahaha")
                .password("lll")
                .avatar("/img/avatar5-sm.jpg")
                .avatar124("/img/avatar5.jpg")
                .back("/img/profon.jpg")
                .country("USA")
                .city("Los-Vegos")
                .build();
        userService.registerUser(dto1);
        UserDto dto2 = UserDto.builder()
                .username("sax")
                .name("Sandre")
                .surname("Salazar")
                .about("im vocalist")
                .password("lll")
                .avatar("/img/avatar4-sm.jpg")
                .avatar124("/img/avatar4.jpg")
                .back("/img/profon.jpg")
                .country("USA")
                .city("Los-Angeles")
                .build();
        userService.registerUser(dto2);
//        User user = new User("micknamsom","hi, i'm Nick","/img/avatar.jpg","/img/profon.jpg","Nick","Manson","USA","Los-Angeloes","/img/avatar124.jpg");

        postService.create(new Post("this guy made new great single, check it out",dtoo,"/img/possst.jpg"));
        postService.create(new Post("my band naw in search of good drums player",dtoo,"/img/possst1.jpg"));
//        postService.create(posts.get(0));

    }

    @RequestMapping(value = "/profile/friends", method = RequestMethod.GET)
    public String getProfileFriendsPage(){
        return "profile-friends";
    }

    @RequestMapping(value = "/profile/about", method = RequestMethod.GET)
    public String getProfileAboutPage(){
        return "profile-about";
    }

    @RequestMapping(value = "/profile/follow", method = RequestMethod.GET)
    public String getProfileFollowPage(){
        return "profile-follow";
    }

    @RequestMapping(value = "/profile/settings", method = RequestMethod.GET)
    public String getProfileSettingsPage(){
        return "profile-settings";
    }
}
