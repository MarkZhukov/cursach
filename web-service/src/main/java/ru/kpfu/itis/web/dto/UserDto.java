package ru.kpfu.itis.web.dto;

import lombok.*;

@Getter
@Builder
@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private String username;
    private String surname;
    private String name;
    private String about;
    private String password;
    private String avatar;
    private String avatar124;
    private String back;
    private String country;
    private String city;
}
